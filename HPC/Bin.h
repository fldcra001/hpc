/*
Bin.h
Contains method headers for binning operations

created 20 June 2015

Craig Feldman
*/

#ifndef BIN_H
#define BIN_H

#include <stdio.h>
#include <fstream>
#include <iostream>
#include <omp.h>
#include <string>


void binData(int* h_array, int xDim, int yDim, std::string fileName);
void binDataParallel(int* h_array, int xDim, int yDim, std::string fileName);
void printBins(int* h_array, int xDim, int yDim, std::string fileName);

#endif