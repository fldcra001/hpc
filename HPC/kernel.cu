/*
Cuda and serial implementations of a median filter
created 19 June 2015
Craig Feldman
*/

#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include "bin.h"
#include <stdio.h>
#include <iostream>
#include <string>
#include <fstream>
#include <algorithm>
#include <chrono>
//#include<vector>
#include<math.h>
//#include <thrust/sort.h>
//#include <thrust/execution_policy.h>

/* --- Choose methods here --- */
#define SERIAL_BIN
#define PARALLEL_BIN
#define SERIAL_MF
#define PARALLEL_MF
//-----------------------------

#define BLOCK_DIM 8

/* --- DEBUG --- */
//#define PRINT_OUTPUT
//#define PRINT_INPUT
// for writing data to disk (should be enabled)
#define WRITE_DATA
//-----------------

cudaError_t cudaMedianFilter(int* arr, int xDim, int yDim, int filterSize);
int* medianFilter(int* initialArr, const int xDim, const int yDim, const int filterSize);

/*
Currently unused
__device__ int quickSelect(int* arr, int start, int end, int k);
__device__ int partition(int* arr, int start, int end);
*/

/* Cuda median filter */
__global__ void medianFilterKernel(int* origArr, int* newArr, int xDim, int yDim, int N, int filterSize) {
	using namespace std;

	// Will store the values that correpsond to the global index of this thread
	__shared__ int s[BLOCK_DIM][BLOCK_DIM];
	//extern __shared__ int s[];
	
	int tx = threadIdx.x;
	int ty = threadIdx.y;

	/*
	int bx = blockIdx.x;
	int by = blockIdx.y;
	int blockId = blockIdx.x + blockIdx.y * gridDim.x;
	int threadId = blockId * (blockDim.x * blockDim.y)
		+ (threadIdx.y * blockDim.x) + threadIdx.x;
	*/
	
	int origX, origY, minX, maxX, minY, maxY;

	// Index of block for median filter
	int windowIndex = 0;

	// get coords of current index
	origX = tx + blockIdx.x * blockDim.x;;
	origY = ty + blockIdx.y * blockDim.y;;

	// Check we are at a valid index
	if (origX >= xDim || origY >= yDim)
		return;

	// Index we are operating on in global array
	int globalIndex = origX + origY *xDim;

	s[ty][tx] = origArr[globalIndex];

	/*
	// Was used for different shared memory method
	s[tx + ty * (BLOCK_DIM + 2 * filterSize)] = origArr[globalIndex];

	if (tx - filterSize < 0)
		s[tx + ty * (BLOCK_DIM + 2 * filterSize)] = origArr[globalIndex - filterSize];
	else 
		s[tx + filterSize + ty * (BLOCK_DIM + 2 * filterSize)] = origArr[globalIndex];
	
	if (tx + filterSize >= BLOCK_DIM)
		s[tx + ty * (BLOCK_DIM + 2 * filterSize)] = origArr[globalIndex + filterSize];
	else
		s[tx - filterSize + ty * (BLOCK_DIM + 2 * filterSize)] = origArr[globalIndex];

	if (ty - filterSize < 0)
		s[tx + ty * (BLOCK_DIM + 2 * filterSize)] = origArr[globalIndex - filterSize*BLOCK_DIM];
	else
		s[tx + (ty + filterSize) * (BLOCK_DIM + 2 * filterSize)] = origArr[globalIndex];

	if (ty + filterSize >= BLOCK_DIM)
		s[tx + ty * (BLOCK_DIM + 2 * filterSize)] = origArr[globalIndex + filterSize*BLOCK_DIM];
	else
		s[tx + (ty-filterSize) * (BLOCK_DIM + 2 * filterSize)] = origArr[globalIndex];
	*/

	// Wait for all threads in block to read data into shared memory
	__syncthreads();

	// Calculate bounds of median filter
	const int offset = (filterSize - 1) / 2;
	minX = max(origX - offset, 0);
	maxX = min(origX + offset, xDim - 1);
	minY = max(origY - offset, 0);
	maxY = min(origY + offset, yDim - 1);
	
	// Windows will store surrounding elements
	int* window = new int[(maxX - minX + 1) * (maxY - minY + 1)];
	// iterate over rows
	for (int y = minY; y <= maxY; ++y) {
		// iterate over columns
		for (int x = minX; x <= maxX; ++x) {

			if (x <= tx && x >= 0 && y <= ty && y >=0) {
				// Can read from shared memory
				window[windowIndex++] = s[y][x];
			//	window[windowIndex++] = origArr[x + y * xDim];
			}
			else
				// Not in shared memory, read from global memory
				window[windowIndex++] = origArr[x + y * xDim];
		}
	}
	// windowIndex is now size of window array

	// QuickSelect the median (modified from - https://gist.github.com/elderdigiuseppe/6242837)
	// ------------------------------
	// k is index of median
	int k = windowIndex / 2;
	int left = 0;
	int right = windowIndex;
	int pivot, pivotValue, storage;
	int temp;

	//we stop when our indicies have crossed
	while (left < right){

		pivot = (left + right) / 2; //this can be whatever
		pivotValue = window[pivot];
		storage = left;

		window[pivot] = window[right];
		window[right] = pivotValue;
		for (int i = left; i < right; ++i){//for each number, if its less than the pivot, move it to the left, otherwise leave it on the right
			if (window[i] < pivotValue){
				temp = window[storage];
				window[storage] = window[i];
				window[i] = temp;
				++storage;
			}
		}
		window[right] = window[storage];
		//move the pivot to its correct absolute location in the list
		window[storage] = pivotValue;

		//pick the correct half of the list you need to parse through to find your K, and ignore the other half
		if (storage < k)
			left = storage + 1;
		else
			right = storage;

	}
	// ------------------------------

	//thrust::sort(thrust::seq, window, window + windowIndex);
	newArr[globalIndex] = window[k];

	//free window memory
	delete[] window;
}

int main(int argc, char* argv[])
{
	using namespace std;

	if (argc != 4) {
		cerr << argc <<  "Incorrect usage. Correct usage is [dim] [filter size] [input file]" << endl;
		return 1;
	}

	const int xDim = atoi(argv[1]);
	// square binning
	const int yDim = xDim;
	const int filterSize = atoi(argv[2]);
	const string inputFileName = argv[3];

	cout << "Process initialised on grid size " << xDim << " x " << yDim << " and file '" << inputFileName << "'" << endl;

	// Allocate memory on the host for bins
	// +1 to take care of fact that precision loss in binning causes value to round to 1
	const int arraySize = xDim * (yDim + 1);
	int* h_array = new int[arraySize];


#ifdef SERIAL_BIN
	std::cout << "\n=== SERIAL BINNING ===\n" << std::endl;

	auto t1 = std::chrono::high_resolution_clock::now();
	binData(h_array, xDim, yDim, inputFileName);
	auto t2 = std::chrono::high_resolution_clock::now();

	std::cout << "\nSerial Binning took "
		<< std::chrono::duration_cast<std::chrono::milliseconds>(t2 - t1).count()
		<< " milliseconds\n";
#endif

#ifdef PARALLEL_BIN
	std::cout << "\n=== OPENMP BINNING ===\n" << std::endl;

	auto t1b = std::chrono::high_resolution_clock::now();

	binDataParallel(h_array, xDim, yDim, inputFileName);

	auto t2b = std::chrono::high_resolution_clock::now();
	std::cout << "\nParallel Binning took "
		<< std::chrono::duration_cast<std::chrono::milliseconds>(t2b - t1b).count()
		<< " milliseconds\n";
#endif

#ifdef PRINT_INPUT
	for (int r = 0; r < yDim; ++r) {
		for (int c = 0; c < xDim; ++c)
			std::cout << h_array[c + r*xDim] << "\t";
		std::cout << std::endl;
	}
#endif

	// Print original data before median filter
#ifdef WRITE_DATA;
	printBins(h_array, xDim, yDim, "output_original.csv");
#endif

#ifdef SERIAL_MF	
	std::cout << "\n=== SERIAL MEDIAN FILTER ===\n" << std::endl;

	// Copy array so that parallel MF can work on original data
	int* tmp_h_array = new int[arraySize];
	std::memcpy(tmp_h_array, h_array, sizeof(int) * arraySize);
	auto t3 = std::chrono::high_resolution_clock::now();

	tmp_h_array = medianFilter(h_array, xDim, yDim, filterSize);

	auto t4 = std::chrono::high_resolution_clock::now();
	std::cout << "\nSerial Median Filter took "
		<< std::chrono::duration_cast<std::chrono::milliseconds>(t4 - t3).count()
		<< " milliseconds\n";

	//----------
	/*
	 t3 = std::chrono::high_resolution_clock::now();

	tmp_h_array = medianFilter(h_array, xDim, yDim, 5);

	 t4 = std::chrono::high_resolution_clock::now();
	std::cout << "\nSerial Median Filter took "
		<< std::chrono::duration_cast<std::chrono::milliseconds>(t4 - t3).count()
		<< " milliseconds\n";

	 t3 = std::chrono::high_resolution_clock::now();

	tmp_h_array = medianFilter(h_array, xDim, yDim, 9);

	 t4 = std::chrono::high_resolution_clock::now();
	std::cout << "\nSerial Median Filter took "
		<< std::chrono::duration_cast<std::chrono::milliseconds>(t4 - t3).count()
		<< " milliseconds\n";

	 t3 = std::chrono::high_resolution_clock::now();

	tmp_h_array = medianFilter(h_array, xDim, yDim, 15);

	 t4 = std::chrono::high_resolution_clock::now();
	std::cout << "\nSerial Median Filter took "
		<< std::chrono::duration_cast<std::chrono::milliseconds>(t4 - t3).count()
		<< " milliseconds\n";

	 t3 = std::chrono::high_resolution_clock::now();

	tmp_h_array = medianFilter(h_array, xDim, yDim, 21);

	 t4 = std::chrono::high_resolution_clock::now();
	std::cout << "\nSerial Median Filter took "
		<< std::chrono::duration_cast<std::chrono::milliseconds>(t4 - t3).count()
		<< " milliseconds\n";
		*/
	//-------------
	#ifdef WRITE_DATA;
	printBins(tmp_h_array, xDim, yDim, "output_serial.csv");
	#endif

	#ifdef PRINT_OUTPUT 
	for (int r = 0; r < yDim; ++r) {
		for (int c = 0; c < xDim; ++c)
			std::cout << tmp_h_array[c + r*xDim] << "\t";
		std::cout << std::endl;
	}
	#endif

#ifndef PARALLEL_MF
	// Copy pointer to original array
	h_array = tmp_h_array;
#endif
#endif

#ifdef PARALLEL_MF
	std::cout << "\n=== CUDA MEDIAN FILTER ===\n" << std::endl;
	cudaFree(0);

	int* a1 = new int[arraySize];
	std::memcpy(a1, h_array, sizeof(int) * arraySize);
	int* a2 = new int[arraySize];
	std::memcpy(a2, h_array, sizeof(int) * arraySize);
	int* a3 = new int[arraySize];
	std::memcpy(a3, h_array, sizeof(int) * arraySize);
	int* a4 = new int[arraySize];
	std::memcpy(a4, h_array, sizeof(int) * arraySize);

	auto t3b = std::chrono::high_resolution_clock::now();

	cudaError_t cudaStatus = cudaMedianFilter(h_array, xDim, yDim, filterSize);
	
	if (cudaStatus != cudaSuccess) {
		cerr << "Cuda Error" << endl;
		exit(1);
	}

	auto t4b = std::chrono::high_resolution_clock::now();

	std::cout << "\nCUDA Median Filter took "
		<< std::chrono::duration_cast<std::chrono::milliseconds>(t4b - t3b).count()
		<< " milliseconds\n";

	//----
	/*
	t3b = std::chrono::high_resolution_clock::now();
	cudaStatus = cudaMedianFilter(a1, 1000, 1000, filterSize);
	if (cudaStatus != cudaSuccess) {
		cerr << "Cuda Error" << endl;
		exit(1);
	}
	t4b = std::chrono::high_resolution_clock::now();
	std::cout << "\nCUDA Median Filter took "
		<< std::chrono::duration_cast<std::chrono::milliseconds>(t4b - t3b).count()
		<< " milliseconds\n";

	t3b = std::chrono::high_resolution_clock::now();
	 cudaStatus = cudaMedianFilter(a2, 4000, 4000, filterSize);
	if (cudaStatus != cudaSuccess) {
		cerr << "Cuda Error" << endl;
		exit(1);
	}
	t4b = std::chrono::high_resolution_clock::now();
	std::cout << "\nCUDA Median Filter took "
		<< std::chrono::duration_cast<std::chrono::milliseconds>(t4b - t3b).count()
		<< " milliseconds\n";


		*/
		

	//-----------

	// cudaDeviceReset must be called before exiting in order for profiling and
	// tracing tools such as Nsight and Visual Profiler to show complete traces.
	cudaStatus = cudaDeviceReset();
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaDeviceReset failed!");
		return 1;
	}

	#ifdef WRITE_DATA;
	printBins(h_array, xDim, yDim, "output_cuda.csv");
	#endif

	#ifdef PRINT_OUTPUT
	for (int r = 0; r < yDim; ++r) {
		for (int c = 0; c < xDim; ++c)
			std::cout << h_array[c + r*xDim] << "\t";
		std::cout << std::endl;
	}
	#endif
#endif

	//printBins(h_array, xDim, yDim, "output.csv");

	delete[] h_array;
    return 0;
}

// Helper function for using Cuda to perform Median Filter
cudaError_t cudaMedianFilter(int* arr, int xDim, int yDim, int filterSize) {
	using namespace std;

	std::cout << "Applying median filter of size " << filterSize << " x " << filterSize << std::endl;;

	int N = xDim * yDim;
	size_t size = N * sizeof(int);

	int *d_orig = 0;
	int *d_new = 0;
	cudaError_t cudaStatus;

	// Choose which GPU to run on, change this on a multi-GPU system.
	cudaStatus = cudaSetDevice(0);
	if (cudaStatus != cudaSuccess) {
		cerr << "cudaSetDevice failed" << endl;
		goto Error;
	}

	// Allocate memory to device
	cudaStatus = cudaMalloc(&d_orig, size);
	if (cudaStatus != cudaSuccess) {
		cerr << "Cuda Error allocating array on device" << endl;
		goto Error;
	}

	cudaStatus = cudaMalloc(&d_new, size);
	if (cudaStatus != cudaSuccess) {
		cerr << "Cuda Error allocating array on device" << endl;
		goto Error;
	}

	// Copy from host to device
	cudaStatus = cudaMemcpy(d_orig, arr, size, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) {
		cerr << "Cuda error copying to device" << endl;
		goto Error;
	}

	dim3 threadsPerBlock(BLOCK_DIM, BLOCK_DIM);
	dim3 numBlocks(ceil((float)xDim / threadsPerBlock.x), ceil((float)yDim / threadsPerBlock.y));
	//size_t sizeofSharedMemoryinBytes = (BLOCK_DIM + 2 * filterSize) * (BLOCK_DIM + 2 * filterSize) * sizeof(int);
	auto t5 = std::chrono::high_resolution_clock::now();

	// Invoke kernel
	cout << "Invoking Kernel" << endl;
	medianFilterKernel <<<numBlocks, threadsPerBlock>>>(d_orig, d_new, xDim, yDim, N, filterSize);

	// Check for any errors launching the kernel
	cudaStatus = cudaGetLastError();
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "Kernel launch failed: %s\n", cudaGetErrorString(cudaStatus));
		goto Error;
	}
	else
		cout << "Successfull kernel launch" << endl;

	// cudaDeviceSynchronize waits for the kernel to finish, and returns
	// any errors encountered during the launch.
	cudaStatus = cudaDeviceSynchronize();
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaDeviceSynchronize returned error code %d after launching Kernel!\n", cudaStatus);
		goto Error;
	}

	auto t6 = std::chrono::high_resolution_clock::now();
	std::cout << "\nCUDA Median Filter excluding initialisation took "
		<< std::chrono::duration_cast<std::chrono::milliseconds>(t6 - t5).count()
		<< " milliseconds";

	// Copy from GPU to host
	cudaStatus = cudaMemcpy(arr, d_new, size, cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMemcpy failed!");
		goto Error;
	}

Error:
	cudaFree(d_orig);
	cudaFree(d_new);

	return cudaStatus;
}

// Serial median filter
int* medianFilter(int* initialArr, const int xDim, const int yDim, const int filterSize) {
	std::cout << "Applying median filter of size " << filterSize << " x " << filterSize << std::endl;;
	// allocate new array
	int* newArr = new int[xDim * yDim];
	int* block = new int[filterSize * filterSize];
	int origX, origY, minX, maxX, minY, maxY;

	// Get elements within filterSize x filterSize block
	for (int i = 0; i < xDim * yDim; ++i) {
		// get coords of current index
		origX = i % xDim;
		origY = i / xDim;
		minX = origX - (filterSize - 1) / 2 ;
		maxX = origX + (filterSize - 1) / 2;
		minY = origY - (filterSize - 1) / 2;
		maxY = origY + (filterSize - 1) / 2;;

		// Check index bounds
		if (minX < 0) minX = 0;
		if (maxX >= xDim) maxX = xDim - 1;
		if (minY < 0) minY = 0;
		if (maxY >= yDim) maxY = yDim - 1;

		// Index of block
		int index = 0;

		// iterate over rows
		for (int r = minX; r <= maxX; ++r) {
			// iterate over columns
			for (int c = minY; c <= maxY; ++c) {
	#ifdef DEBUG_MEDIAN_S
				std::cout <<"X: " <<  r  << "y: " << c << std::endl;
				std::cout << "Accessing :  " << initialArr[r + c * xDim];
	#endif
				block[index++] = initialArr[r + c * xDim];
			}
		}

		// replace with median
		int n = index / 2;
		std::sort(block, block + index);

	#ifdef DEBUG_MEDIAN_S
		std::cout << "index = " << index << std::endl;
		std::cout << "Setting to :  " << block[n];
	#endif
		newArr[i] = block[n];
	}

	delete[] block;

	return newArr;
	//printBins(newArr, xDim, yDim);

}



/*
__device__ int quickSelect(int* arr, int start, int end, int k) {
	//return 0;
	int s = partition(arr, 0, end);
	if (s == k - 1)
		return arr[s];
	else if (s > start + k - 1)
		quickSelect(arr, start, s, k);
	else
		quickSelect(arr, s + 1, end, k - 1 - s);
}
__device__ int partition(int* arr, int start, int end) {
	int i = start;
	int j = end;
	int pivot = arr[start];
	while (i < j){
		while (arr[i] < pivot && i != end - 1)
			++i;
		while (j != 0 && arr[j] > pivot)
			--j;

		int tmp = arr[j];
		arr[j] = arr[i];
		arr[i] = tmp;
	}
	int tmp = arr[j];
	arr[j] = arr[i];
	arr[i] = tmp;

	tmp = arr[j];
	arr[j] = arr[start];
	arr[start] = tmp;
	
		return j;
}
*/