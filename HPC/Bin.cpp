/* 
Provides Methods for Binning Data and printing the bins to disk
created 19 June 2015
Craig Feldman
*/

#include "bin.h"

//#define DEBUG_BINNING
//#define DEBUG_PRINT

// Bins data in serial
void binData(int* h_array, int xDim, int yDim, std::string fileName) {
	std::cout << "Binning data (serial)" << std::endl;

	// +1 to take care of fact that precision loss in binning causes value to round to 1
	const int arraySize = xDim * (yDim + 1);

	// intitialise bins
	for (int i = 0; i < arraySize; ++i)
		h_array[i] = 0;

	std::ifstream input(fileName, std::ios::binary);
	if (!input) {
		std::cerr << "Input file not found" << std::endl;
		exit(1);
	}

	float x, y;
	int count = 0;
	while (!input.eof()) {

		input.read((char*)&x, sizeof(float));
		input.read((char*)&y, sizeof(float));

		// Calculate binning indexes
		int xBin = x * xDim;
		int yBin = y * yDim;

		int i = xBin + yBin*yDim;

#ifdef DEBUG_BINNING
		using namespace std;
		if (i >= xDim * yDim) {
			cerr << "Error in binning. index out of range. " << i << endl;
			cout << "X: " << x << endl;
			cout << "Y: " << y << endl;
			cout << "xBin: " << xBin << endl;
			cout << "yBin: " << yBin << endl;
			cout << "calc xBin: " << x * xDim << endl;
			cout << "calc yBin: " << y * yDim << endl;
			//i = 0;
			//exit(1);
		}
#endif
		// Incrementt the bin
		++h_array[i];
		++count;
	}

	std::cout << "Read a total of " << count-1 << " (x,y) value pairs" << std::endl;
}

// Bins data in parallel using OpenMP
void binDataParallel(int* h_array, int xDim, int yDim, std::string fileName) {
	using namespace std;
	cout << "Binning data (OpenMP)" << endl;

	// +1 to take care of fact that precision loss in binning causes value to round to 1
	const int arraySize = xDim * (yDim + 1);

	std::string filePath = fileName;
	ifstream input(filePath, ios::binary | ios::ate);
	if (!input) {
		cerr << "Input file not found" << endl;
		exit(1);
	}


	// Size of file in bytes
	// Note: might need to manually enter the file size in bytes for very large files
	// auto fileSize = 5600000000; //(for 7.0e+08)
	auto fileSize = input.tellg();
	
	// 8 bytes per pair
	if (fileSize % 8 != 0)
		cerr << "Warning, input file appears to be incorrect" << endl;

	// How many threads to spawn
	int numThreads = omp_get_num_procs();
	// How many pairs to read
	int numToRead = (fileSize / numThreads) / 8;

	cout << "File size: " << fileSize << " bytes" <<  endl;
	cout << "Number of threads for OpenMP: " << numThreads << endl;
	cout << "Number of (x,y) values for each thread to read: " << numToRead << endl;

	// Set to start of file
	input.clear();
	input.close();
	//input.seekg(0, ios::beg);

	// intitialise bins
	for (int i = 0; i < arraySize; ++i)
		h_array[i] = 0;

	float x, y;
	int count = 0;

	// Have each thread read a section of the file
#pragma omp parallel for num_threads(numThreads) private (x, y)
	for (int i = 0; i < numThreads; ++i) {
		ifstream fin(fileName, ios::binary);

		fin.seekg(i * numToRead * 2);
		for (int j = 0; j < numToRead && !fin.eof(); ++j) {
			fin.read((char*)&x, sizeof(float));
			fin.read((char*)&y, sizeof(float));
//std::cout << "hello from thread " << omp_get_thread_num() << std::endl;;
			// Calculate binning indexes
			int xBin = x * xDim;
			int yBin = y * yDim;

#pragma omp atomic
			++count;

#ifdef DEBUG_BINNING
			if (index >= xDim * yDim) {
				cerr << "Error in binning. index out of range. " << index << endl;
				cout << "X: " << x << endl;
				cout << "Y: " << y << endl;
				cout << "xBin: " << xBin << endl;
				cout << "yBin: " << yBin << endl;
				cout << "calc xBin: " << x * xDim << endl;
				cout << "calc yBin: " << y * yDim << endl;
				//i = 0;
				//exit(1);
			}
#endif

			int index = xBin + yBin*yDim;
#pragma omp atomic
			// incrment the bin
			++h_array[index];
		}
	}
	cout << "Read a total of " << count << " (x,y) value pairs" << endl;
}

// Outputs the bins to disk
void printBins(int* h_array, int xDim, int yDim, std::string filename){
	std::cout << "\nWriting data to csv file '" << filename << "'" << std::endl;
	float xInterval = (float)1 / xDim;
	float yInterval = (float)1 / yDim;

	// Starting values
	float x = xInterval / 2;
	float y = yInterval / 2;

	std::ofstream outFile(filename);
	for (int r = 0; r < xDim; ++r) {
		outFile << ",";
		outFile << x;
		x += xInterval;
	}

	for (int c = 0; c < yDim; ++c) {
		outFile << std::endl;
		outFile << y;
		y += yInterval;

		for (int r = 0; r < xDim; ++r) {
			outFile << ",";
			outFile << h_array[r + xDim*c];
		}
	}

	outFile.close();

#ifdef DEBUG_PRINT
	for (int r = 0; r < yDim; ++r) {
		for (int c = 0; c < xDim; ++c)
			std::cout << h_array[c + r*xDim] <<  " , ";
		std::cout << std::endl;
	}
#endif

	std::cout << "\nDONE" << std::endl;


}