**Additional information can be found in the downloads section**

Applies a 2D median filter to data using both a serial and CUDA implementation.

**********
High Performance Computing

19 July 2015
**********

Import solution into Visual Studio.
Run with the following command line paramters: [dim] [filter size] [input file]
Where:
	dim = number of bins in x/y dimension
	filter size = size of filter in x/y dimension (must be odd)
	input file = name of input file

Ensure that input files are in the 'HPC' folder. This is also where output csv files are written to.

Currently both the serial and parallel implementations of binning and the median filter run by default.
You can select which methods to run in the kernel.cu file.

kernel.cu contains the main method and median filter implementations.
Bin.cpp contains the binning methods.